import numpy as np
import itertools, sys, json
import experiment.util as util
import os.path

outputPath = "results/tuningStats/output.dat"
sizeRange = range(5,20)
if len(sys.argv) > 1:
	sizeRange = range(int(sys.argv[1]), int(sys.argv[2]))
	outputPath = sys.argv[3]

nValues = np.concatenate((
	np.arange(0.1, 0.3, 0.05),
	np.arange(0.3, 0.4, 0.02),
	np.arange(0.4, 0.8, 0.01),
	np.arange(0.8, 0.95, 0.02)),
	axis = 0)

rValues = np.arange(0.1, 0.95, 0.05)

out = open(outputPath, 'w')
out.write('Size range: ' + str(sizeRange) + '\n')
out.write('nValues: ' + str(nValues) + '\n')
out.write('rValues: ' + str(rValues) + '\n')

def accept(v, N, R):
	return reduce(lambda x, y: x + 1 if y > N else 0, v) / float(len(v)) >= R

def result(comparisons, N, R):
	cAcc, cRej, iAcc, iRej = 0, 0, 0, 0
	for profileId, values in comparisons.items():
		for userId, v in values.items():
			shouldAcc = profileId == userId
			acc = accept(v, N, R)
			if acc and shouldAcc: cAcc += 1
			elif not acc and not shouldAcc: cRej += 1
			elif not acc: iRej += 1
			else: iAcc += 1
	FAR = iAcc / float(iAcc + cRej)
	FRR = iRej / float(iRej + cAcc)
	#return util.accuracy(cAcc, cRej, iAcc, iRej)
	return util.precision_recall_hm(cAcc, iRej, iAcc)
	#return util.harmonicMean(FAR, FRR)

def getInputFiles(sampleSize):
	directory = 'results/statsComparsions/'
	idx = 0
	result = []
	while True:
		path = directory + str(sampleSize) + '_' + str(idx) + '.json'
		print path
		if os.path.isfile(path):
			result.append(open(path, 'r'))
		else:
			return result
		idx += 1

directory = 'results/'

for sampleSize in sizeRange:
	best = {(N, V): [] for N,V in itertools.product(nValues, rValues)}
	for infile in getInputFiles(sampleSize):
		data = json.load(infile)
		print "data loaded"

		beforeCount = len(best.keys())
		for N, R in best.keys():
			r = result(data, N, R)
			if r == 0: #eliminacja
				del best[(N,R)]
				print "removing " + str((N,R))
			else:
				best[(N,R)].append(r)
		print "done, before: " + str(beforeCount) + " after: " + str(len(best.keys()))

	final = {k: (np.mean(v), np.std(v)) for k,v in best.items()}
	sfinal = sorted(final.items(), key=lambda x: x[1], reverse=True)
	out.write('Top 10 best results (for SIZE ' + str(sampleSize) + '):\n')
	for x in range(10):
		row = sfinal[x]
		out.write(str(x) + '.\t' + str(row[0][0]) + ', ' + str(row[0][1]) + '\t' + str(row[1]) + '\n')
	out.flush()
out.close()

from experiment.profileManager import ProfileManager
import methods.statistic.ngramProfile as statistic
import sys, json

#default parameters
profileSource = "data/users/small.json"
outputPath = "results/statsComparsions"
sizeRange = range(5,20)
repetitions = 10
methods = [statistic.NGramProfile]

#reading parameters
if len(sys.argv) > 1:
	sizeRange = range(int(sys.argv[1]), int(sys.argv[2]))
	outputPath = sys.argv[3]

print "Producing comparsions for statistic approach for"
print "size range: " + str(sizeRange)
print "repetitions: " + str(repetitions)
print "profile source: " + profileSource
print "output: " + outputPath

pm = ProfileManager(profileSource, methods)

for sampleSize in sizeRange:
	print "Sample size: " + str(sampleSize)
	for repetition in range(repetitions):
		total = {}
		profiles = pm.divideBySampleSize(sampleSize)[statistic.NGramProfile]
		allProfiles = sum(profiles.values(), [])
		print "#" + str(repetition + 1) + "/" + str(repetitions)
		print "calculating comparisons of many profiles: " + str(len(allProfiles))
		for X in allProfiles:
			sys.stdout.write('X')
			sys.stdout.flush()
			comparisons = {k: [] for k in profiles.keys()}
			for Y in allProfiles:
				if X == Y: continue
				comparisons[Y.userId].append(statistic.NGramProfile.compareDetailed(X, Y))
			total[X.userId] = {k: comparisons[k] for k in filter(lambda x: len(comparisons[x]) > 0, comparisons.keys())}

		# saving to file
		fileName = str(sampleSize) + "_" + str(repetition) + '.json'
		with open(outputPath + '/' + fileName, 'w') as out:
			json.dump(total, out)

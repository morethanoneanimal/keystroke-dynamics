from experiment.profileManager import ProfileManager
from experiment.output import Output
import experiment.util as util
from experiment.identification import *
import json, time, sys, config
import pandas as pd

profileSource = "data/users/all.json"
#profileSource = "data/users/small.json"
sizeRange = range(20, 21)
repetitions = config.repetitions
methods = config.methods

#reading parameters
if len(sys.argv) > 1:
	sizeRange = range(int(sys.argv[1]), int(sys.argv[2]))
	outputPath = sys.argv[3]

pm = ProfileManager(profileSource, methods)

for sampleSize in sizeRange:
	print "sampleSize: " + str(sampleSize)
	profiles = pm.divideBySampleSize(sampleSize)
	out = Output(sampleSize, 'authentication')
	out.makeIntro(util.profilesCount(profiles), util.identifiedProfilesCount(profiles), methods)
	for method in methods:
		method.InitParameters(sampleSize)

	for repetition in range(repetitions):
		profiles = pm.divideBySampleSize(sampleSize)
		print "#" + str(repetition + 1) + "/" + str(repetitions)
		identification = IdentificationTest(profiles)
		results = identification.identify()
		util.tagResults(results, repetition)

		out.storeResults(results)

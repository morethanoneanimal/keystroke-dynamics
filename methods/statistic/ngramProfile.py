from methods.profile import Profile
import numpy as np
import math

class NGramProfile(Profile):
	ShortStr = "stat"
	N = 0.33
	R = 0.2
	n = 2

	def __init__(self, userId):
		super(NGramProfile, self).__init__(userId)
		self.ngrams = {}
		self.allKeystrokes = []

	def learn(self, samples):
		self.ngrams = {}
		sinceLastSpace = 0
		self.allKeystrokes = []
		for sample in samples:
			sampleNgrams = filter(lambda ngram: NGramProfile.isValid(ngram) and ngram.time < 700, sample.getNGrams(NGramProfile.n))
			self.allKeystrokes += sampleNgrams
			for ngram in sampleNgrams:
				if sinceLastSpace < 6:
					if ngram.value not in self.ngrams:
						self.ngrams[ngram.value] = []
					self.ngrams[ngram.value].append(ngram.time)
				if ngram.value[1] == ' ':
					sinceLastSpace = 0
				else:
					sinceLastSpace += 1
		array = np.array(map(lambda x: x.time, self.allKeystrokes))
		self.mean = array.mean()
		self.std = array.std()
		self.__generateMeanTable()

	def __generateMeanTable(self):
		self.stats = {}
		for key, value in self.ngrams.items():
			tab = np.array(value)
			self.stats[key] = (tab.mean(), tab.std())

	@staticmethod
	def InitParameters(size):
		NGramProfile.N, NGramProfile.R = NGramProfile.parameters[size]

	@staticmethod
	def Classify(profileX, allProfiles):
		q = {NGramProfile._avgDistance(profileX, allProfiles[key]): key for key in allProfiles.keys()}
		bestVal = max(q.keys())
		return q[bestVal]

	@staticmethod
	def _avgDistance(X, profiles):
		return sum(map(lambda p: NGramProfile.compareDetailed(X, p), profiles)) / float(len(profiles))

	@staticmethod
	def Identify(profile, allProfiles):
		return {k: NGramProfile.__identify(profile, v) for k, v in allProfiles.items() }

	@staticmethod
	def __identify(profile, profiles):
		r = sum(map(lambda x: 1 if NGramProfile.compare(profile, x) else 0, profiles) ) / float(len(profiles))
		return r >= NGramProfile.R

	@staticmethod
	def compareOverall(reference, test):
		val = abs( (test.mean - reference.mean) / (test.std / math.sqrt(len(test.allKeystrokes))) )
		return val

	@staticmethod
	def compareDetailed(reference, test):
		valid = 0
		total = 0
		for ngram in test.allKeystrokes:
			if ngram.value in reference.stats:
				mean, std = reference.stats[ngram.value][0], reference.stats[ngram.value][1]
				m1, m2 = mean - std/2, mean + std/2
				if m1 <= ngram.time <= m2:
					valid += 1
				total += 1
		return valid/float(total)

	@staticmethod
	def compare(reference, test):
		detailed = NGramProfile.compareDetailed(reference, test)
		return detailed >= NGramProfile.N

	@staticmethod
	def isValid(ngram):
		return all( map(lambda x: x.isalpha() or x.isspace(), ngram.value) )
	parameters = {
		1: (0.1, 0.15),
		2: (0.15, 0.90),
		3: (0.2, 0.9),
		4: (0.25, 0.9),
		5: (0.3, 0.8),
		6: (0.3, 0.9),
		7: (0.3, 0.9),
		8: (0.32, 0.9),
		9: (0.34, 0.9),
		10: (0.34, 0.9),
		11: (0.34, 0.9),
		12: (0.36, 0.85),
		13: (0.36, 0.85),
		14: (0.36, 0.9),
		15: (0.38, 0.8),
		16: (0.38, 0.75),
		17: (0.38, 0.75),
		18: (0.38, 0.75),
		19: (0.4, 0.75),
		20: (0.4, 0.75),
		21: (0.4, 0.6),
		22: (0.4, 0.6),
		23: (0.4, 0.85),
		24: (0.41, 0.65),
		25: (0.41, 0.75),
		26: (0.42, 0.45),
		27: (0.42, 0.9),
		28: (0.42, 0.6),
		29: (0.43, 0.55),
		30: (0.42, 0.5),
		31: (0.43, 0.7),
		32: (0.43, 0.7),
		33: (0.43, 0.7),
		34: (0.44, 0.6)
	}

import abc
class Profile:
	__metaclass__ = abc.ABCMeta

	def __init__(self, userId):
		self.userId = userId

	@abc.abstractmethod
	def learn(self, samples):
		pass

	@staticmethod
	def Classify(profile, allProfiles):
		raise Expception('Not implemented yet')

	@staticmethod
	def Identify(profile, allProfiles):
		raise Expception('Not implemented yet')

	@staticmethod
	def InitParameters(size):
		pass

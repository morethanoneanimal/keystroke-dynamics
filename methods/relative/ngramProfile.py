import experiment.sample, itertools, numpy
import pickle
from pandas import *
from methods.profile import Profile

class NGramProfile(Profile):
	ShortStr = "rel"
	k = 0.81
	a = 2.1
	b = 2.5
	n = 3

	def __init__(self, userId):
		self.userId = userId
		self.__tab = {}
		self.times = []
		self.ngramsLearned, self.ngramsAnalyzed = 0, 0

	def learn(self, samples):
		for sample in samples:
			self.__learn(sample)
		self.analyze()
	
	def __learn(self, sample):
		sample = sample.keylog
		for idx in xrange(len(sample) - (NGramProfile.n-1)):
			ngram = ''.join(map(lambda x: sample[x].char, range(idx, idx+NGramProfile.n)))
			time = sum(map(lambda x: sample[x].time, range(idx+1, idx+NGramProfile.n)))

			#self.df.loc[len(self.df)] = [trigraph, time]
			if ngram not in self.__tab:
				self.__tab[ngram] = []
			self.__tab[ngram].append(time)
			self.ngramsLearned += 1

	def analyze(self):
		self.__tab = sorted(self.__tab.items(), reverse=True, key=lambda x:len(x[1]))
		idx = 0
		for key, val in self.__tab:
			val = filter(lambda x: x < 1000, val)
			if len(val) != 0:
				self.times.append( NGram(key, val) )
				self.ngramsAnalyzed += 1
			idx += 1
		self.times = filter(lambda x: len(x.times) >= 3, self.times)
		self.times = sorted(self.times, key=lambda x: x.avg)

	def saveDescription(self, path):
		with open(path, 'w') as output:
			for x in range( min(len(self.times), 50)):
				q = self.times[x]
				for time in q.times:
					output.write(q.ngram.replace(',', '@') + ',' + str(x) + ',' + str(time) + '\n')

	def save(self, path):
		if self.times == None:
			raise BaseException('Call analyze first!')
		with open(path, 'w') as output:
			pickle.dump(self, output)

	@staticmethod
	def compare(entity1, entity2):
		result, matched = 0, 0
		idx = 0
		leftTimes, rightTimes = NGramProfile.getCommonSubset(entity1.times, entity2.times)
		#print leftTimes, rightTimes
		for ngram in leftTimes:
			f = NGramProfile.distanceInVector(ngram, idx, rightTimes)
			if f >= 0:
				result += f
				matched += 1
			idx += 1
		maxDisorder = matched**2/2
		return (result/float(maxDisorder) if maxDisorder > 0 else 0, matched)

	@staticmethod
	def getCommonSubset(times1, times2):
		rightValues = {k:v for (k,v) in map(lambda x: (x.ngram, x.avg), times2)}
		leftResult = []
		rightResult = []
		for ngram in times1:
			if ngram.ngram in rightValues:
				leftResult.append(ngram.ngram)
				rightResult.append( ngram.ngram )
		return leftResult, sorted(rightResult, key = lambda x: rightValues[x])

	@staticmethod
	def InitParameters(size):
		NGramProfile.a, NGramProfile.b, NGramProfile.k = NGramProfile.parameters[size]

	@staticmethod
	def __Classification(profile, allProfiles):
		return sorted( [(NGramProfile.__countMeanDistance(profile, allProfiles[key]), key) for key in allProfiles.keys()] )

	@staticmethod
	def Classify(profile, allProfiles):
		return NGramProfile.__Classification(profile, allProfiles)[0][1]

	@staticmethod
	def _secondStageParameters(profiles):
		n = len(profiles)
		distances = [ [NGramProfile.compare(profiles[x], profiles[y])[0] for y in range(x+1, n)] for x in range(n)]
		d = [0] * len(profiles)
		for x in range(n):
			leftSide, rightSide = 0, 0
			for i in range(len(distances)):
				for k in range(len(distances[i])):
					if i == x or k == x:
						leftSide += distances[i][k]
					else:
						rightSide += distances[i][k]
			leftSide /= (n-1)
			if n == 2:
				rightSide = 0
			else:
				rightSide /= (n*(n-1)/2) - (n-1)
			d[x] = abs( leftSide - rightSide )
		maxD = max(d)
		std = numpy.std(d)
		return maxD, std
	
	@staticmethod
	def _firstStage(mdAX, mA, mdBX):
		return mdAX < mA + NGramProfile.k * abs(mdBX - mA)

	@staticmethod
	def _secondStage(mdAX, mA, maxD, std):
		return mdAX < mA + NGramProfile.a * maxD + NGramProfile.b * std

	@staticmethod
	def _firstStageParameters(profile, allProfiles):
		classification = NGramProfile.__Classification(profile, allProfiles)
		mdAX, AKey = classification[0]
		mdBX, _ = classification[1]
		mA = NGramProfile.__countM(allProfiles[AKey])
		AProfiles = allProfiles[AKey]
		return mdAX, mdBX, mA, AProfiles

	@staticmethod
	def Identify(profile, allProfiles):
		shouldAccept = False
		mdAX, mdBX, mA, AProfiles = NGramProfile._firstStageParameters(profile, allProfiles)
		AKey = AProfiles[0].userId
		n = len(AProfiles)
		#pierwsze kryterium
		#if mdAX < mA + NGramProfile.k * abs(mdBX - mA):
		if NGramProfile._firstStage(mdAX, mA, mdBX):
			if n > 1:
				maxD, std = NGramProfile._secondStageParameters(AProfiles)
				#if mdAX < mA + NGramProfile.a * maxD + NGramProfile.b * std:
				if NGramProfile._secondStage(mdAX, mA, maxD, std):
					shouldAccept = True
			else:
				shouldAccept = True
		if shouldAccept:
			return {key: key == AKey for key in allProfiles.keys()}
		else:
			return {key: False for key in allProfiles.keys()}

	@staticmethod
	def __countMeanDistance(profile, profiles):
		return sum(NGramProfile.compare(profile, x)[0] for x in profiles) / len(profiles) if len(profiles) > 0 else 1

	@staticmethod
	def __countM(profiles):
		combinations = list(itertools.combinations(profiles, 2))
		if len(combinations) == 0:
			return 0
		else:
			return sum( map(lambda x: NGramProfile.compare(x[0], x[1])[0], combinations)) / len(combinations)

	@staticmethod
	def distanceInVector(ngram, srcIdx, times):
		idx = -1
		for v in times:
			idx += 1
			if ngram == v:
				return abs(srcIdx - idx)
		return -1

	@staticmethod
	def load(path):
		with open(path, 'r') as input:
			return pickle.load(input)

	parameters = {
		1: (3.7, 4.1, 0.39),
		2: (3.6, 4.0, 0.38),
		3: (3.6, 4.0, 0.38),
		4: (3.6, 4.0, 0.34),
		5: (3.6, 4.0, 0.37),
		6: (3.6, 4.0, 0.4),
		7: (3.4, 3.9, 0.34),
		8: (3.4, 3.7, 0.35),
		9: (3.3, 3.5, 0.49),
		10: (3.3, 3.5, 0.38),
		11: (3.2, 3.5, 0.41),
		12: (3.2, 3.5, 0.38),
		13: (3.0, 3.4, 0.43),
		14: (3.0, 3.4, 0.39),
		15: (3.0, 3.3, 0.38),
		16: (2.9, 3.2, 0.86),
		17: (2.9, 3.1, 0.89),
		18: (2.9, 3.0, 0.89),
		19: (2.8, 3.0, 0.89),
		20: (2.7, 3.0, 0.88),
		21: (2.7, 3.0, 0.86),
		22: (2.7, 3.0, 0.88),
		23: (2.7, 3.0, 0.86),
		24: (2.7, 2.1, 0.73),
		25: (2.7, 2.0, 0.73),
		26: (2.7, 1.7, 0.78),
		27: (2.7, 1.7, 0.88),
		28: (2.6, 1.5, 0.83),
		29: (2.4, 1.5, 0.89),
		30: (2.3, 1.5, 0.89),
		31: (2.3, 1.5, 0.79),
		32: (2.3, 1.2, 0.84),
		33: (2.3, 1.1, 0.79),
		34: (2.3, 1.1, 0.65),
		35: (1.3, 1.0, 0.51)
		}


class NGram():
	def __init__(self, ngram, times):
		self.ngram = ngram
		self.times = times
		self.avg = sum(times) / float(len(times))
	
	def __str__(self):
		return "'" + self.ngram + "': " + str(self.avg) + '(' + str(len(self.times)) + ')'

	def __repr__(self):
		return self.__str__()

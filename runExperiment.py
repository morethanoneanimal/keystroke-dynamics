import subprocess, argparse
import os, time, sys

parser = argparse.ArgumentParser()
parser.add_argument('-C', action='store_const', dest='type',
		const='C',
		help='Run classification')
parser.add_argument('-I', action='store_const', dest='type',
		const='I',
		help='Run classification')
parser.add_argument('-o', action='store', dest='output_path',
		help='Output path (directory)')
parser.add_argument('-n', action='store', dest='cores', default=2, type=int, 
		help='Number of processes to create')
parser.add_argument('-r', action='store', dest='lower_range', default=1, type=int,
		help='Lower range (default 1)')
parser.add_argument('-R', action='store', dest='upper_range', default=35, type=int,
		help='Upper range (default 35)')

var = parser.parse_args()
if var.type is None:
	print "Identification (-I) or classification (-C)?"
	exit()

sizes = range(var.lower_range, var.upper_range)
scriptToRun = 'runClassification.py' if var.type == 'C' else 'runIdentification.py'
FNULL = open(os.devnull, 'w')

# does path ends up with '/' char?
if len(var.output_path) > 0:
	if var.output_path[-1] != '/':
		var.output_path = var.output_path + '/'

processes = [None] * var.cores

print "Running " + 'classification' if var.type == 'C' else 'identification' + ", range " + str(sizes) + ", cores: " + str(len(processes))

idx = 0

while idx < len(sizes):
	for x in range(len(processes)):
		if idx >= len(sizes):
			break
		if processes[x] is None or processes[x].poll() is not None:
			size = sizes[idx]
			#parameters = ['python', 'tuneRelative/produceComparisons.py', str(size), str(size+1), 'results/relComparisons/' + str(size) + '.csv']
			parameters = ['python', scriptToRun, str(size), str(size+1),
					var.output_path + 'output_' + str(size) + '.csv']
			#processes[x] = subprocess.Popen(parameters, stdout=FNULL)
			processes[x] = subprocess.Popen(parameters)
			print "running " + str(parameters)

			idx += 1
	time.sleep(5)

print "waiting for processes to finish"

for x in processes:
	if x is not None:
		x.wait()

print "done"

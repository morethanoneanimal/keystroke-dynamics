# Keystroke dynamics platform #

Platform and data for running experiments on continous Keystroke Dynamics.

### Requirements ###

* Python 2.7
* Numpy
* Pandas

### Architecture ###

Project structure is fairly simple.
First of all, there is a **data** directory containing, as one could guess, data used in experiments.

Directory **experiment** contains all classes required to run an experiment.
There is a code which is responsible for loading and managing input data (*profileManager.py*),
classification and identification implementation (respectively *classification.py* and *identification.py*),
class which represents sample (*sample.py*) etc.

*method* directory is a place where code for Keystroke Dynamics algorithms are stored.
Also, there are base classes for those implementations.

### Running ###

To run experiment you should use *runExperiment.py* script.
It's simple tool that allows you to select type of experiment (classification or identification),
output path where results are stored, range (explained below) and also number of processes used.

### How to add new algorithm ###

To add new algorithm you should implement it in **methods** directory (actually it's not necessary).
Your method should define a profile (and inherit after *methods/profile.py*), which can be trained by learning from text samples.

As a second step you should add new method to list of methods in *config.py*.

### Dictionary ###

Few concepts explained in here.

**User profile** - profile trained with set of text samples from one user. Every KD method defines what is contains and how to compare it with other profiles.

**Range** - User profile can be trained with some amount of text samples. Typically, the more samples the better. So by defining range, let's say from 1 to 7, you run series of experiments to see how does methods perform when profiles are trained with 1, 2, 3, ... 7 samples per profile.

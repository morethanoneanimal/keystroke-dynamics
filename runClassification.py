from experiment.profileManager import ProfileManager
from experiment.output import Output
import experiment.util as util
from experiment.classification import *
import pandas as pd
import json, time, sys
import config

profileSource = "data/users/all.json"
#profileSource = "data/users/small.json"
sizeRange = range(8, 9)
outputPath = 'sth.dat'
methods = config.methods
repetitions = config.repetitions
results = pd.DataFrame(columns = ('size', 'method', 'correct', 'incorrect', 'total', 'correctProc', 'incorrectProc', 'time'))

#reading parameters
if len(sys.argv) > 1:
	sizeRange = range(int(sys.argv[1]), int(sys.argv[2]))
	outputPath = sys.argv[3]

pm = ProfileManager(profileSource, methods)
idx = 0

for sampleSize in sizeRange:
	print "sample size: " + str(sampleSize)
	out = Output(sampleSize, 'classification')

	for repetition in range(repetitions):
		profiles = pm.divideBySampleSize(sampleSize)
		print "#" + str(repetition + 1) + "/" + str(repetitions)

		classification = ClassificationTest(profiles, sampleSize)
		result = classification.classify()
		for key, val in result.items():
			results.loc[idx] = (sampleSize, key.ShortStr, val.correct, val.incorrect, val.total, val.correctPerCent, val.incorrectPerCent, val.time)
			idx += 1

results.to_csv(outputPath, index_label='id')

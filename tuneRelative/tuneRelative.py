from experiment.profileManager import ProfileManager
from experiment.output import Output
import experiment.util as util
from identification import *
import methods.relative.ngramProfile as relative
import numpy as np
import itertools, sys

#default parameters
profileSource = "data/users/all.json"
outputPath = "results/tuningRelative/output.dat"
sizeRange = range(3,4)
repetitions = 1
method = relative.NGramProfile

#reading parameters
if len(sys.argv) > 1:
	sizeRange = range(int(sys.argv[1]), int(sys.argv[2]))
	outputPath = sys.argv[3]

kValues = np.arange(0.2, 0.9, 0.01)

aValues = np.arange(1.0, 3.8, 0.1)
bValues = np.arange(1.0, 4.2, 0.1)

def accept(var):
	if method._firstStage(var["mdAX"], var["mA"], var["mdBX"]):
		if var["n"] <= 1: return True
		else:
			return method._secondStage(var["mdAX"], var["mA"], var["maxD"], var["std"])
	else:
		return False

def result(total, A, B, K):
	cAcc, cRej, iAcc, iRej = 0, 0, 0, 0
	for profile, var in total.items():
		key = var["key"]
		shouldAcc = key == profile.userId
		relative.NGramProfile.a = A
		method.a = A
		method.b = B
		method.k = K
		acc = accept(var)
		if acc and shouldAcc: cAcc += 1
		elif not acc and not shouldAcc: cRej += 1
		elif not acc: iRej += 1
		else: iAcc += 1
	#FAR = iAcc / float(iAcc + cRej)
	#FRR = iRej / float(iRej + cAcc)
	return util.precision_recall_hm(cAcc, iRej, iAcc)
	#return util.accuracy(cAcc, cRej, iAcc, iRej)

print "Running tuning RELATIVE approach for"
print "size range: " + str(sizeRange)
print "repetitions: " + str(repetitions)
print "aValues: " + str(aValues)
print "bValue: " + str(bValues)
print "profile source: " + profileSource
print "output: " + outputPath

out = open(outputPath, 'w')
out.write('Size range: ' + str(sizeRange) + '\n')
out.write('Repetitions ' + str(repetitions) + '\n')
out.write('aValues: ' + str(aValues) + '\n')
out.write('bValues: ' + str(bValues) + '\n')

pm = ProfileManager(profileSource, [method])

for sampleSize in sizeRange:
	total = {}
	best = {(A, B, K): [] for A,B,K in itertools.product(aValues, bValues, kValues)}
	print "Sample size: " + str(sampleSize)
	profiles = pm.divideBySampleSize(sampleSize)
	results = []
	for repetition in range(repetitions):
		profiles = pm.divideBySampleSize(sampleSize)[method]
		allProfiles = sum(profiles.values(), [])
		print "#" + str(repetition + 1) + "/" + str(repetitions)
		print "calculating comparisons of profiles: " + str(len(allProfiles))
		for X in allProfiles:
			sys.stdout.write('X')
			sys.stdout.flush()
			profilesWithoutX = util.allProfilesWithoutX(profiles, X)
			
			mdAX, mdBX, mA, AProfiles = method._firstStageParameters(X, profilesWithoutX)
			if len(AProfiles) > 1:
				maxD, std = method._secondStageParameters(AProfiles)
			else:
				maxD, std = None, None
			total[X] = {"mdAX": mdAX, "mdBX": mdBX, "mA": mA, "maxD": maxD, "std": std, "n": len(AProfiles), "key": AProfiles[0].userId, "atackerOnly": len(profiles[X.userId]) <= 1}
		print "\ncalculating results...",
		sys.stdout.flush()
		for A, B, K in itertools.product(aValues, bValues, kValues):
			best[(A,B,K)].append(result(total, A, B, K))
		print "done"

	final = {k: (np.mean(v), np.std(v)) for k,v in best.items()}
	sfinal = sorted(final.items(), key=lambda x: (x[1], x[0]), reverse=True)
	head = min(25000, len(sfinal))
	out.write('Top ' + str(head) + ' best results:\n')
	for x in range(head):
		row = sfinal[x]
		out.write(str(x) + '.\t' + str(row[0][0]) + ', ' + str(row[0][1]) + ', ' + str(row[0][2]) + '\t' + str(row[1]) + '\n')
	out.flush()

	for v in sfinal[0:250]:
		print v

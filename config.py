import methods.relative.ngramProfile as relative
import methods.statistic.ngramProfile as statistic

methods = [relative.NGramProfile, statistic.NGramProfile]
repetitions = 10

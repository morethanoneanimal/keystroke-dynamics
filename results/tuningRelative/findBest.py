class Node:
	def __init__(self, key, lvl):
		self.key = key
		self.val = 0
		self.prev = None
		self.lvl = lvl
	
	def __str__(self):
		return str(self.key) + ":\t" + str(self.val)

def distance(a, b):
	return sum(map(lambda x: abs(a.key[x] - b.key[x]), range(len(a.key))))

def getFileName(x):
	prefix, postfix = 'hm_output_output_', '.dat'
	return prefix + str(x) + postfix

def getFile(x):
	return open(getFileName(x), 'r')

def skipLines(f, lines):
	for i in range(lines):
		f.readline()

def updateNode(node, prevNodes):
	bestNode, bestDistance = None, None
	for pn in prevNodes:
		d = pn.val + distance(node, pn)
		if bestDistance == None or d < bestDistance:
			bestDistance = d
			node.prev = pn
	node.val = bestDistance

def parseFile(f, num):
	line = f.readline()
	nodes = []
	bestVal = None
	while line is not '':
		parts = line.replace(',', '').replace('(', '').split()
		if len(parts) == 0:
			break
		key = ( float(parts[1]), float(parts[2]), float(parts[3]) )
		val = float(parts[4])

		if bestVal is None: bestVal = val
		if val < bestVal:
			break

		nodes.append(Node(key, num) )
		line = f.readline()
	return nodes

sizes = range(1, 36)
prevNodes = [ Node( (0,0,0), 0) ]

for x in sizes:
	print "calculating" + str(x)
	f = getFile(x)
	skipLines(f, 8)
	nodes = parseFile(f, x)

	for node in nodes:
		updateNode(node, prevNodes)
	prevNodes = nodes

nodes = sorted(nodes, key=lambda x: x.val, reverse = True)
print "final"
x = 0
for node in nodes[0:100]:
	print str(node)

print "path"
i = nodes[0]
while i.prev is not None:
	print str(i)
	i = i.prev


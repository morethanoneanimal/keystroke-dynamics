import pandas as pd

sizeRanege = range(1, 35)
outRel = open('rel_analyze.csv', 'w')
outStat = open('stat_analyze.csv', 'w')
headerCreated = False

def getFile(x):
	return str(x)+'spp_0.csv'

def analyze(df, out, size):
	out.write( str(size) + ',' + ','.join(map(str, df.mean())) + ',' + ','.join(map(str, df.std())) + '\n')

def makeHeader(df, out, size):
	out.write('size,' + ','.join(df.mean().keys()) + ',' + ','.join(map(lambda x: x+'_std', df.std().keys())) + '\n')

for size in sizeRanege:
	print size
	df = pd.read_csv(getFile(size))
	gr = df.groupby('method')
	rel, stat = gr.get_group('rel'), gr.get_group('stat')

	if not headerCreated:
		makeHeader(rel, outRel, size)
		makeHeader(stat, outStat, size)
		headerCreated = True

	analyze(rel, outRel, size)
	analyze(stat, outStat, size)

import pandas as pd

rel = pd.read_csv('rel_analyze.csv')
stat = pd.read_csv('stat_analyze.csv')

for x in rel.index:
	# 1 & 1551 & \multicolumn{1}{c|}{132 (14\%)} & 12 & 43 & 23 \\ 
	r = rel.ix[x]
	s = stat.ix[x]
	total = int(r['cAcc'] + r['cRej'] + r['iAcc'] + r['iRej'])
	print '{:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{ {:.1f} ({:.3f}\%)}} & {:.1f} ({:.3f}\%) & {:.1f} ({:.3f}\%) & {:.1f} ({:.3f}\%) \\\\'.format(r['size'],
			total,
			r['iAcc'], r['iAcc']/total, r['iRej'], r['iRej']/total,
			s['iAcc'], s['iAcc']/total, s['iRej'], s['iRej']/total)

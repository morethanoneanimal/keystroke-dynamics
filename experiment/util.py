def profilesCount(profiles):
	return sum( map(len, profiles.values()) )

def identifiedProfilesCount(profiles):
	return profilesCount( {k: profiles[k] for k in filter(lambda x: len(profiles[x]) > 1, profiles.keys()) } )

def tagResults(results, tag):
	for result in results:
		result.id = tag

def harmonicMean(x, y):
	if min(x, y) == 0:
		return max(x, y)
	else:
		return 2/(1/x + 1/y)

def precision_recall_hm(cAcc, iRej, iAcc):
	p, r = precision_recall(cAcc, iRej, iAcc)
	return harmonicMean(p, r)
def precision_recall(cAcc, iRej, iAcc):
	return precision(cAcc, iAcc), recall(cAcc, iRej)

def precision(cAcc, iAcc):
	return cAcc / float(cAcc+iAcc) if cAcc+iAcc > 0 else 0

def recall(cAcc, iRej):
	return cAcc / float(cAcc+iRej) if cAcc+iRej > 0 else 0

def accuracy(cAcc, cRej, iAcc, iRej):
	return (cAcc + cRej) / float(cAcc + cRej + iAcc + iRej)

def allProfilesWithoutX(allProfiles, X):
	copy = allProfiles.copy()
	copy[X.userId] = list(copy[X.userId])
	del copy[X.userId][ copy[X.userId].index(X) ]
	return copy

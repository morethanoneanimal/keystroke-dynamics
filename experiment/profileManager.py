import pandas as pd
import methods.relative.ngramProfile, methods.statistic.ngramProfile
from sample import Sample
import random, datetime

class ProfileManager:
	def __init__(self, path='data/users/all.json', profileClasses = [methods.relative.ngramProfile.NGramProfile, methods.statistic.ngramProfile.NGramProfile]):
		self.df = pd.read_json(path)
		self.profileClasses = profileClasses
		self.grouped = {}
		for idx, record in self.df.iterrows():
			userId = record['User']
			if userId not in self.grouped: self.grouped[userId] = []
			self.grouped[userId].append(Sample(userId, record['Keylog'], record['Date'], record['Accuracy']))

	def dataDescription(self, id=None):
		if not id: self.__allDataDescription()
		else: self.__dataDescription(id)

	def __allDataDescription(self):
		for idx, group in self.grouped.items():
			lengths = map(len, group)
			print("UserId: " + str(idx) + ', samples: ' + str(len(group)) + '\t(min: ' + str(min(lengths)) + ', max: ' + str(max(lengths)) + ', avg: ' + str( sum(lengths)/float(len(lengths))) + ')')

	def __dataDescription(self, id):
		data = self.grouped[id]
		idx = 0
		for sample in data:
			print('SampleIdx: ' + str(idx) + ', size: ' + str(len(sample)))
			idx += 1
		print('Samples for user ' + str(id) + ': ' + str(len(data)))

	def removeSamples(self, userId):
		del self.grouped[userId]

	def divideBySampleSize(self, sampleSize):
		result = {}
		for idx, group in self.grouped.items():
			result[idx] = self.__divideGroupBySampleSize(group, sampleSize)
			if len(result[idx]) == 0:
				del result[idx]
		def samplesToProfiles(samples, profileClass):
			profile = profileClass(samples[0].userId)
			profile.learn(samples)
			return profile

		return {p: {k: map(lambda x: samplesToProfiles(x, p), v) for k,v in result.items()} for p in self.profileClasses}

	def getClassificator(self, profiles):
		return Classificator(profiles, self.profileClass)

	def __divideGroupBySampleSize(self, group, sampleSize):
		result = []
		idxs = range(len(group))
		random.shuffle(idxs)
		idx = 0
		while idx + sampleSize <= len(group):
			result.append([])
			for x in range(idx, idx+sampleSize):
				result[-1].append( group[idxs[x]] )
			idx += sampleSize
		return result

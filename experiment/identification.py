import time
import experiment.util as util

class IdentificationTest():
	def __init__(self, profiles):
		self.profiles = profiles
		self.profileClasses = self.profiles.keys()
		self.__allProfiles = {k: sum(v.values(), []) for k,v in self.profiles.items()}
	
	def identify(self):
		return map(self.__identify, self.profileClasses)

	def __identify(self, profileClass):
		self.errorMatrix = {}
		self.cAcc, self.cRej, self.iAcc, self.iRej = 0, 0, 0, 0
		startTime = time.clock()
		for X in self.__allProfiles[profileClass]:
			data = self.__prepareData(X)
			result = profileClass.Identify(X, data)
			for key, access in result.items():
				if key != X.userId:
					if not access:
						self.cRej += 1
					else:
						self.iAcc += 1
						self.__addError(X.userId, key)
				else:
					if access:
						self.cAcc += 1
					else:
						self.iRej += 1
						self.__addError(X.userId, key)
		q = lambda x, total: str(x) + " (" + str( x*100/float(total)) + "%)"
		#print "Algorithm: " + profileClass.ShortStr
		#print "correctAccepted:\t" + q(self.cAcc, self.cAcc + self.iRej)
		#print "correctRejected:\t" + q(self.cRej, self.cRej + self.iAcc)
		#print "incorrecAccepted:\t" + q(self.iAcc, self.cRej + self.iAcc)
		#print "incorrectRejected:\t" + q(self.iRej, self.cAcc + self.iRej)
		return IdentificationResult(profileClass.ShortStr, (self.cAcc, self.cRej, self.iAcc, self.iRej), self.errorMatrix, time.clock() - startTime)

	def __prepareData(self, X):
		data = self.profiles[type(X)].copy()
		data[X.userId] = list(data[X.userId])
		del data[X.userId][ data[X.userId].index(X) ]
		# if there is no item left - remove whole list
		if not data[X.userId]:
			del data[X.userId]
		return data

	def __addError(self, X, key):
		k = (X, key)
		if k not in self.errorMatrix:
			self.errorMatrix[k] = 1
		else:
			self.errorMatrix[k] += 1

class IdentificationResult():
	def __init__(self, method, results, errorMatrix, time):
		self.method = method
		self.correctlyAccepted, self.correctlyRejected, self.incorrectlyAccepted, self.incorrectlyRejected = results
		self.total = sum(results)
		self.errorMatrix = errorMatrix
		self.time = time
		self.id = 'x'
	
		self.FAR = self.incorrectlyAccepted / float(self.incorrectlyAccepted + self.correctlyRejected)
		self.FRR = self.incorrectlyRejected / float(self.incorrectlyRejected + self.correctlyAccepted)
		self.precision, self.recall = util.precision_recall(self.correctlyAccepted, self.incorrectlyRejected, self.incorrectlyAccepted)
		self.F1 = util.harmonicMean(self.precision, self.recall)
		self.accuracy = util.accuracy(self.correctlyAccepted, self.correctlyRejected, self.incorrectlyAccepted, self.incorrectlyRejected)
		if min(self.FAR, self.FRR) == 0: # if there is zero you can't compute harmonic mean
			self.harmonicMean = max(self.FAR, self.FRR)
		else:
			self.harmonicMean = 2.0 / (1/self.FAR + 1/self.FRR)

	def __cmp__(self, other):
		return self.harmonicMean.__cmp__(other.harmonicMean)

	def __str__(self):
		return "FAR: " + str(self.FAR * 100) + "% FRR: " + str(self.FRR * 100) + " hm: " + str(self.harmonicMean)

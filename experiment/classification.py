import time
import experiment.util as util

class ClassificationTest():
	def __init__(self, profiles, size):
		self.size = size
		self.profiles = profiles
		self.profilesClasses = self.profiles.keys()
		self.profilesToClassify = {key: ClassificationTest._findProfilesToClassify(profiles[key]) for key in profiles.keys()}

	def classify(self):
		return {key: self.__classify(key) for key in self.profilesClasses}

	def __classify(self, profileClass):
		profileClass.InitParameters(self.size)
		profiles = self.profilesToClassify[profileClass]
		correct, incorrect = 0,0
		startTime = time.clock()

		for X in profiles:
			data = util.allProfilesWithoutX(self.profiles[profileClass], X)
			result = profileClass.Classify(X, data)
			if X.userId == result:
				correct += 1
			else:
				incorrect += 1

		return ClassificationResult(profileClass.ShortStr, (correct, incorrect), time.clock()-startTime)

	@staticmethod
	def _findProfilesToClassify(profiles):
		bigEnoughUsers = filter(lambda x: len(profiles[x]) > 1, profiles.keys())
		return sum( map(lambda x: profiles[x], bigEnoughUsers), [])

class ClassificationResult():
	def __init__(self, method, results, time):
		self.method = method
		self.correct, self.incorrect = results
		self.total = sum(results)
		self.time = time

		self.correctPerCent = float(self.correct) / self.total
		self.incorrectPerCent = float(self.incorrect) / self.total

	def __cmp__(self, other):
		return self.correctPerCent.__cmp__(other.correctPerCent)

	def __str__(self):
		return "Correct: " + str(self.correctPerCent) + ", incorrect: "  + str(self.incorrectPerCent)

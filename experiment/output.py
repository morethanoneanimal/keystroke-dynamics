import time, csv
import os.path

class Output():
	def __init__(self, sampleSize, problem):
		self.sampleSize = sampleSize
		self.problem = problem

	def makeIntro(self, profilesCount, identifiedProfilesCount, methods):
		self.path = self.__generatePath(self.sampleSize)
		try:
			os.makedirs(os.path.dirname(self.path))
		except:
			pass
		self.__makeDescription(profilesCount, identifiedProfilesCount, methods)
		self.__prepareCsv(methods)

	def storeResults(self, results):
		with open(self.path, 'a') as out:
			writer = csv.writer(out)
			for result in results:
				writer.writerow( map(str, [result.id,
					result.method,
					result.correctlyAccepted,
					result.correctlyRejected,
					result.incorrectlyAccepted,
					result.incorrectlyRejected,
					result.FAR,
					result.FRR,
					result.harmonicMean,
					result.precision,
					result.recall,
					result.F1,
					result.accuracy,
					result.time]) )

	def __makeDescription(self, profilesCount, identifiedProfilesCount, methods):
		with open(self.path+'.decs', 'w') as out:
			out.write("ProfilesCount: " + str(profilesCount) + '\n')
			out.write("IdentifiedProfilesCount: " + str(identifiedProfilesCount) + '\n')
			out.write("Methods: " + ', '.join(map(lambda x: x.ShortStr, methods)))

	def __prepareCsv(self, methods):
		with open(self.path, 'w') as out:
			writer = csv.writer(out)
			#writer.writerow( sum([map(lambda x: x + methodShort, prefixes) for methodShort in map(lambda q: q.ShortStr, methods)], []) )
			writer.writerow( ['id',
				'method',
				'cAcc',
				'cRej',
				'iAcc',
				'iRej',
				'FAR',
				'FRR',
				'hm',
				'precision',
				'recall',
				'F1',
				'Acc',
				'time'] )


	def __generatePath(self, sampleSize):
		pre = "results/" + self.problem + '/' + time.strftime("%d_%m_%y") + '/'
		getPath = lambda x: pre + str(sampleSize) + 'spp_' + str(x) + '.csv'
		x = 0
		while os.path.isfile(getPath(x)):
			x += 1
		return getPath(x)

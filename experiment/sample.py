class Char:
	def __init__(self, char, time):
		self.char, self.time = char, time
	
	def __str__(self):
		return "'" + char + "': " + str(time)

class NGram:
	def __init__(self, chars):
		self.value = reduce(lambda x, y: x + y.char, chars, '')
		self.time = reduce(lambda x, y: x + y.time, chars, 0) - chars[0].time
		self.n = len(chars)


def parseKeylog(keylog):
	result = []
	char = None
	miliseconds = []
	idx = 0
	originalText = ''
	for letter in keylog:
		if letter.isdigit():
			miliseconds.append(letter)
		else:
			if char is not None:
				if miliseconds:
					result.append( Char(char, int( ''.join(miliseconds) )))
					originalText += char
				else:
					raise BaseException('Two consectuive letters, at ' + str(idx))
			miliseconds = []
			char = letter
		idx += 1
	return result, originalText


class Sample:
	def __init__(self, userId, keylog, date, accuracy = None, wordsPerMinute = None):
		self.userId = userId
		self.rawKeylog = keylog
		self.keylog, self.originalText = parseKeylog(keylog)
		self.date = date
		self.accuracy = accuracy
		self.wordsPerMinute = wordsPerMinute

	def __len__(self):
		return len(self.parsedKeylog)

	def getNGrams(self, n):
		ngrams = []
		for idx in range(len(self.keylog)-(n-1)):
			ngrams.append( NGram([self.keylog[idx + x] for x in range(n)]) )
		return ngrams
